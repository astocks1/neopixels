import board
import time
import pika
import neopixel
import random

pixels = neopixel.NeoPixel(board.D18, 70)

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

channel.queue_declare('msges')

led_ammt = 70

def red():
	pixels.fill((155,0,0))
	print('running red function')

def green():
	pixels.fill((0,155,0))
	print('running green function')

def blue():
	pixels.fill((0,0,155))
	print('running blue function')

def cyan():
	pixels.fill((0,155,155))
	print('running cyan function')

def magenta():
	pixels.fill((155,0,155))
	print('running cyan function')

def randsig():
	for i in range(1, 50):
		print(i)
		signal_red = random.randint(1,255)
		signal_green= random.randint(1,255)
		signal_blue = random.randint(1,255)
		for j in range(0,led_ammt):
			pixels[j]=(signal_red, signal_green, signal_blue)
			pixels[j-6]=(signal_red*0.3, signal_green*0.3, signal_blue*0.3)
			time.sleep(0.0005)
			
def midoff():
	for i in range(1,50):
		print(i)
		signal_red = random.randint(1,255)
		signal_green= random.randint(1,255)
		signal_blue = random.randint(1,255)
		pixels[35]=((signal_red,signal_green,signal_blue))
		for j in range(1,int(led_ammt/2)):
			pixels[35+j]=((signal_red,signal_green,signal_blue))
			pixels[35-j]=((signal_red,signal_green,signal_blue))
			time.sleep(0.0005)

def randred():
	for i in range(1,40):
		print(i)
		signal_red = random.randint(1,255)
		pixels.fill((signal_red,0,0))
		time.sleep(random.randint(1,100)/1000)

def fade_in():
	for i in range(0,70):
		pixels.fill((i,i*0.8,0))
	pixels.fill((70,70*0.8,0))

def fade_out():
	for i in reversed(range(0,70)):
		pixels.fill((i,i*0.8,0))

def glow():
	for i in range(1,10):
		fade_in()
		fade_out()

def off():
	pixels.fill((0,0,0))
	print('running off function')
	
	
dispatcher = {'red':red, 'green':green, 'blue':blue, 'cyan':cyan,
	'magenta':magenta, 'fade_in':fade_in, 'fade_out':fade_out, 'randsig': randsig, 'midoff':midoff, 'glow':glow, 'randred':randred, 'off':off}

def callback(ch, method, properties, body):
	if body.decode("utf-8") in dispatcher:
		dispatcher[body.decode("utf-8")]()
	else:
		print('Color non-existent')
	
channel.basic_consume(queue='msges',  auto_ack=True, on_message_callback=callback)

print(' [*] Waiting for messages. To exit please press CTRL+C')

channel.start_consuming()

connection.close()
