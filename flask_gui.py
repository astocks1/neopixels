import time
import pika
from markupsafe import escape
from random import randrange
from flask import Flask, render_template, request

app = Flask(__name__)


def send_message(ch, msg):
	connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
	channel = connection.channel()
	channel.queue_declare(ch)
	channel.basic_publish(exchange = '', routing_key=ch,body=escape(msg))
	connection.close()

@app.route('/')
def index(code=None):
	return 'Select Color'

@app.route('/color/<string:code>')
def fill_red(code):
	send_message('msges',code)
	return render_template('gui.html', code=code)


@app.route('/set_red')
def set_red():
	red=request.args.get("red")
	msg = 'r' + red
	send_message('msges', msg)
	print('Red', red)
	return "Received " + str(red)


  	
 
@app.route('/off')
def all_off():
	send_message('msges','off')
	return 'all_off'

